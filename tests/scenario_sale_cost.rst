===================
Sale cost Scenario
===================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale_cost::

    >>> config = activate_modules('sale_cost')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)

Create new user::

    >>> Group = Model.get('res.group')
    >>> admin_group, = Group.find([('name', '=', 'Administration')])
    >>> user = User()
    >>> user.name = 'User 1'
    >>> user.login = 'user1'
    >>> user.company = None
    >>> user.groups.append(admin_group)
    >>> user.save()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> address = customer.addresses.new()
    >>> address.name = 'Address 2'
    >>> customer.save()
    >>> shipment_party = Party(name='Shipment party')
    >>> address2 = shipment_party.addresses.new()
    >>> address2.name = 'Address 3'
    >>> shipment_party.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.categories.append(category)
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'product 2'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service = template.products[0]
    >>> unit.digits = 2
    >>> unit.rounding = 0.01
    >>> unit.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_comm = CostType(name='Commission')
    >>> type_comm.product = service
    >>> type_comm.formula = '0.2*unknown_amount'
    >>> type_comm.apply_method
    'none'
    >>> type_comm.apply_method = 'sale'
    >>> bool(type_comm.include_on_price)
    False
    >>> type_comm.save()
    Traceback (most recent call last):
        ...
    simpleeval.NameNotDefined: 'unknown_amount' is not defined for expression 'Decimal ('0.2')*unknown_amount '
    >>> type_comm.formula = '0.2*untaxed_amount'
    >>> type_comm.quantity_formula
    '1'
    >>> type_comm.quantity_formula = 'quantity'
    >>> type_comm.save()
    >>> type_comm.distribution_method
    'untaxed_amount'
    >>> type_comm.apply_method
    'sale'
    >>> type_comm.apply_point
    'on_confirm'
    >>> type_others = CostType(name='Others')
    >>> type_others.product = service
    >>> type_others.formula = '10.0*quantity'
    >>> type_others.distribution_method = 'quantity'
    >>> type_others.apply_method = 'none'
    >>> type_others.save()

Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template_other = CostTemplate()
    >>> template_other.type_ = type_others
    >>> template_other.formula
    '10.0*quantity'
    >>> template_other.distribution_method
    'quantity'
    >>> template_other.save()
    >>> template_comm = CostTemplate()
    >>> template_comm.type_ = type_comm
    >>> template_comm.party = customer
    >>> template_comm.formula
    '0.2*untaxed_amount'
    >>> template_comm.distribution_method
    'untaxed_amount'
    >>> template_comm.save()
    >>> template = CostTemplate()
    >>> template.type_ = type_comm
    >>> template.formula = '0.1*untaxed_amount'
    >>> template.party = customer
    >>> template.shipment_party = shipment_party
    >>> template.shipment_address = shipment_party.addresses[1]
    >>> template.save()

Create an Inventory::

    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new(product=product1)
    >>> inventory_line.quantity = 100.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory_line = inventory.lines.new(product=product2)
    >>> inventory_line.quantity = 20.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Sale 2 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product2
    >>> sale_line.quantity = 100.0
    >>> sale_line.unit = gram
    >>> sale.click('quote')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('23.00'), Decimal('2.30'), Decimal('25.30'))

Check sale costs::

    >>> len(sale.costs)
    2
    >>> comm, = [c for c in sale.costs if c.type_.id == type_comm.id]
    >>> comm.template == template_comm
    True
    >>> comm.formula
    '0.2*untaxed_amount'
    >>> comm.distribution_method
    'untaxed_amount'
    >>> comm.amount
    Decimal('4.60')
    >>> comm.quantity
    2.1
    >>> other, = [c for c in sale.costs if c.type_.id == type_others.id]
    >>> other.template == template_other
    True
    >>> other.amount
    Decimal('21.00')
    >>> bool(comm.applied)
    False
    >>> bool(comm.appliable)
    False
    >>> Cost = Model.get('sale.cost')
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '=', False)]))
    True
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '!=', False)]))
    False
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '=', True)]))
    False
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '!=', True)]))
    True
    >>> bool(other.applied)
    False
    >>> bool(other.appliable)
    False

Change quantity on sale line 2::

    >>> sale.click('draft')
    >>> sline, = [l for l in sale.lines if l.product == product2]
    >>> sline.quantity = 300.0
    >>> sale.click('quote')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('29.00'), Decimal('2.90'), Decimal('31.90'))

Check sale costs::

    >>> comm.reload()
    >>> comm.amount
    Decimal('5.80')
    >>> comm.quantity
    2.3
    >>> bool(comm.appliable)
    False
    >>> other.reload()
    >>> other.amount
    Decimal('23.00')
    >>> bool(other.appliable)
    False

Add other cost with products in cost type::

    >>> CostType = Model.get('sale.cost.type')
    >>> cost_type = CostType(name='Cost Type with Products')
    >>> cost_type.product = service
    >>> cost_type.formula = '0.2*untaxed_amount'
    >>> cost_type.manual = True
    >>> product1 = Product(product1.id)
    >>> cost_type.products.append(product1)
    >>> cost_type.save()
    >>> cost = sale.costs.new()
    >>> cost.type_ = cost_type
    >>> cost.amount
    Decimal('4.00')
    >>> cost.save()

Check cost applying::

    >>> other.reload()
    >>> not other.lines
    True
    >>> bool(other.undistributed)
    True
    >>> len(sale.lines)
    3
    >>> len([l for c in sale.costs for l in c.lines])
    0
    >>> sale.click('confirm')
    >>> len(sale.lines)
    4
    >>> service_line, = [l for l in sale.lines if l.product and l.product.type == 'service']
    >>> service_line.quantity
    2.3
    >>> service_line.unit_price
    Decimal('-2.5217')
    >>> service_line.product == service
    True
    >>> service_line.cost.id == comm.id
    True
    >>> sale.untaxed_amount_precost_cache, sale.total_amount_precost_cache
    (Decimal('29.00'), Decimal('31.90'))
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('23.20'), Decimal('2.32'), Decimal('25.52'))
    >>> sale.untaxed_amount_cache, sale.total_amount_cache
    (Decimal('23.20'), Decimal('25.52'))
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '=', False)]))
    False
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '!=', False)]))
    True
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '=', True)]))
    True
    >>> bool(Cost.find([('id', '=', comm.id), ('applied', '!=', True)]))
    False
    >>> comm.reload()
    >>> bool(comm.appliable)
    False
    >>> other.reload()
    >>> bool(other.applied)
    False
    >>> bool(other.appliable)
    False

Check after process::

    >>> sale.state
    'processing'
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('23.20'), Decimal('2.32'), Decimal('25.52'))
    >>> sale.state
    'processing'
    >>> len(sale.shipments), len(sale.shipment_returns), len(sale.invoices)
    (1, 0, 1)
    >>> invoice, = sale.invoices
    >>> invoice.origins == sale.rec_name
    True
    >>> shipment, = sale.shipments
    >>> shipment.origins == sale.rec_name
    True

Check cost distribution::

    >>> sale.click('distribute_costs')
    >>> len([l for c in sale.costs for l in c.lines])
    5
    >>> comm.reload()
    >>> comm.lines[0].amount, comm.lines[1].amount
    (Decimal('4.00'), Decimal('1.80'))
    >>> other.reload()
    >>> other.amount
    Decimal('23.00')
    >>> other.lines[0].amount, other.lines[1].amount
    (Decimal('20.00'), Decimal('3.00'))
    >>> bool(other.undistributed)
    False
    >>> cost.reload()
    >>> cost_line, = cost.lines
    >>> cost_line.amount
    Decimal('4.00')
    >>> sale_line1 = sale.lines[0]
    >>> sale_line1.costs_amount
    Decimal('28.00')
    >>> sale_line2 = sale.lines[2]
    >>> sale_line2.costs_amount
    Decimal('4.80')

Edit distribution::

    >>> all(l.amount for l in comm.lines)
    True
    >>> comm.lines_edit = True
    >>> not any(l.amount for l in comm.lines)
    True
    >>> comm.lines[0].factor, comm.lines[1].factor
    (20.0, 9.0)
    >>> comm.lines[0].factor = 9.0
    >>> comm.save()
    >>> comm.reload()
    >>> all(l.amount for l in comm.lines)
    True
    >>> comm.lines[0].factor, comm.lines[1].factor
    (9.0, 9.0)
    >>> comm.lines[0].amount, comm.lines[1].amount
    (Decimal('2.90'), Decimal('2.90'))
    >>> comm.lines_edit = False
    >>> comm.lines[0].amount, comm.lines[1].amount
    (Decimal('4.00'), Decimal('1.80'))
    >>> all(l.factor is None for l in comm.lines)
    True
    >>> sale.reload()

Add manual costs on processed sale::

    >>> type_manual = CostType(name='Manual')
    >>> type_manual.product = service
    >>> type_manual.formula = '2.0*quantity'
    >>> type_manual.distribution_method = 'quantity'
    >>> type_manual.apply_method = 'none'
    >>> type_manual.manual = True
    >>> type_manual.save()
    >>> cost = sale.costs.new()
    >>> cost.type_ = type_manual
    >>> sale.save()
    >>> len(sale.costs)
    4
    >>> sale.cost_amount
    Decimal('37.40')
    >>> sale.costs.remove(other)
    >>> sale.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot delete cost "Others @ 1" due to it is not manual and Sale is in advance state. - 

Distribute manual cost, modify amount and distribute again::

    >>> sale.reload()
    >>> sale.click('distribute_costs')
    >>> manual_cost, = [c for c in sale.costs if c.type_.id == type_manual.id]
    >>> bool(manual_cost.lines)
    True
    >>> sum(l.amount for l in manual_cost.lines)
    Decimal('4.60')
    >>> manual_cost.amount = Decimal('5.5')
    >>> manual_cost.save()
    >>> bool(manual_cost.lines)
    True
    >>> sum(l.amount for l in manual_cost.lines)
    Decimal('5.50')

Configure none distribution method::

    >>> sale.click('distribute_costs')
    >>> bool(manual_cost.lines)
    True
    >>> manual_cost.distribution_method = 'none'
    >>> manual_cost.save()
    >>> manual_cost.reload()
    >>> bool(manual_cost.lines)
    False
    >>> sale.click('distribute_costs')
    >>> manual_cost.reload()
    >>> bool(manual_cost.lines)
    False

Delete manual cost::

    >>> sale.costs.remove(manual_cost)
    >>> sale.save()

Cron distribute sale costs when sale processed::

    >>> cost = sale.costs.new()
    >>> cost.type_ = type_manual
    >>> sale.save()
    >>> cost, = [cost for cost in sale.costs if cost.type_ == type_manual]
    >>> bool(cost.lines)
    False
    >>> Cron = Model.get('ir.cron')
    >>> Company = Model.get('company.company')
    >>> cron_distribute_costs, = Cron.find([
    ...     ('method', '=', 'sale.sale|cron_distribute_costs'),
    ...     ('active', '=', False)])
    >>> cron_distribute_costs.companies.append(Company(company.id))
    >>> cron_distribute_costs.save()
    >>> cron_distribute_costs.click('run_once')
    >>> cost.reload()
    >>> bool(cost.lines)
    True

Admin user::

    >>> User = Model.get('res.user')
    >>> admin_user = User(config.user)

Create sale user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> sale_user = User()
    >>> sale_user.name = 'Sale'
    >>> sale_user.login = 'sale'
    >>> sale_user.company = company
    >>> sale_group, = Group.find([('name', '=', 'Sales')])
    >>> sale_user.groups.append(sale_group)
    >>> sale_user.save()
    >>> set_user(sale_user)

Check sale line::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale.click('quote')
    >>> sale.click('draft')
    >>> sale.delete()

Check sale cost rule::

    >>> SaleCost = Model.get('sale.cost')
    >>> len(SaleCost.find([]))
    4
    >>> config.user = user.id
    >>> len(SaleCost.find([]))
    0
    >>> config.user = admin.id
    >>> len(SaleCost.find([]))
    4

Cost Type::

    >>> cost_type = CostType(name='Manual')
    >>> cost_type.product = service
    >>> cost_type.formula = '-2.0*quantity'
    >>> cost_type.distribution_method = 'quantity'
    >>> cost_type.apply_method = 'none'
    >>> cost_type.manual = True
    >>> cost_type.save()

Other New Sale processing::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> cost = sale.costs.new()
    >>> cost.type_ = cost_type
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.save()
    >>> len(sale.costs)
    1
    >>> sale.state
    'processing'

Other New Sale2 draft::

    >>> sale2 = Sale()
    >>> sale2.party = customer
    >>> sale2.invoice_method = 'order'
    >>> sale2.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale2.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> cost = sale2.costs.new()
    >>> cost.type_ = cost_type
    >>> sale2.save()
    >>> len(sale2.costs)
    1
    >>> sale2.state
    'draft'

Other New Sale3 quotation::

    >>> sale3 = Sale()
    >>> sale3.party = customer
    >>> sale3.invoice_method = 'order'
    >>> sale3.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale3.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> cost = sale3.costs.new()
    >>> cost.type_ = cost_type
    >>> sale3.save()
    >>> len(sale3.costs)
    1
    >>> sale3.state
    'draft'
    >>> sale3.click('quote')
    >>> sale3.save()
    >>> sale3.state
    'quotation'

Other New Sale4::

    >>> sale4 = Sale()
    >>> sale4.party = customer
    >>> sale4.shipment_party = customer
    >>> sale4.invoice_method = 'order'
    >>> sale4.sale_date = today
    >>> sale_line = SaleLine()
    >>> sale4.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> cost = sale4.costs.new()
    >>> cost.type_ = cost_type
    >>> cost.sale_party == sale4.party
    True
    >>> cost.sale_shipment_party == sale4.shipment_party
    True
    >>> sale4.save()
    >>> len(sale4.costs)
    1
    >>> sale4.state
    'draft'
    >>> sale4.click('quote')
    >>> sale4.click('cancel')
    >>> sale4.save()
    >>> sale4.state
    'cancelled'

Change formula with wizard::

    >>> cost_type.formula = '-4.0*quantity'
    >>> cost_type.save()

    >>> change_formula = Wizard('sale.cost.change_formula', [cost_type])
    >>> change_formula.form.from_date = today
    >>> change_formula.form.to_date = today
    >>> change_formula.form.update_templates = True
    >>> change_formula.execute('modify')

Check sales::

    >>> sale.costs[0].formula
    '-4.0*quantity'
    >>> sale.costs[0].amount
    Decimal('-8.00')
    >>> sale2.costs[0].formula
    '-4.0*quantity'
    >>> sale2.costs[0].amount
    Decimal('-8.00')
    >>> sale3.costs[0].formula
    '-4.0*quantity'
    >>> sale3.costs[0].amount
    Decimal('-8.00')
    >>> sale4.costs[0].formula
    '-2.0*quantity'
    >>> sale4.costs[0].amount
    Decimal('-4.00')

Create Sale with invoice party::

    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_party = supplier
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product2
    >>> sale_line.quantity = 100.0
    >>> sale_line.unit = gram

Add cost price to sale::

    >>> invoice_type = CostType(name='Sale invoice')
    >>> invoice_type.product = service
    >>> invoice_type.formula = '10.0*untaxed_amount'
    >>> invoice_type.distribution_method = 'quantity'
    >>> invoice_type.apply_method = 'sale_invoice'
    >>> invoice_type.manual = True
    >>> invoice_type.save()
    >>> sale_cost = sale.costs.new()
    >>> sale_cost.type_ = invoice_type
    >>> sale.save()

Create invoice::

    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> invoice, = sale.invoices
    >>> invoice.party == supplier
    True
    >>> invoice_cost_line, = [l for l in invoice.lines 
    ...     if 'sale.cost' in str(l.origin)]
    >>> invoice_cost_line.quantity
    1.0
    >>> invoice_cost_line.amount == sale.untaxed_amount * Decimal('-10.0')
    True
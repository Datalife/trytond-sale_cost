# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta
from trytond.model import fields, Model
from trytond.pyson import Eval, Bool
from trytond.modules.product import price_digits


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    amount_with_cost = fields.Function(
        fields.Numeric('Amount with cost',
            digits=(16, Eval('_parent_invoice', {}).get('currency_digits', 2)),
            ),
        'get_amount_with_cost')
    unit_price_with_cost = fields.Function(
        fields.Numeric('Unit price with cost', digits=price_digits,
            ),
        'get_unit_price_with_cost')
    unit_price_readonly = fields.Function(
        fields.Boolean('Unit price readonly'),
        'get_unit_price_readonly')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        if cls.unit_price.states.get('readonly'):
            cls.unit_price.states['readonly'] |= Bool(
                Eval('unit_price_readonly'))
        else:
            cls.unit_price.states['readonly'] = Bool(
                Eval('unit_price_readonly'))

    def get_amount_with_cost(self, name):
        if self.type == 'line':
            return self.currency.round(
                Decimal(self.quantity) * self.unit_price_with_cost)

    def get_unit_price_with_cost(self, name):
        if (self.origin
                and isinstance(self.origin, Model)
                and self.origin.__name__ == 'sale.line'):
            return self.origin.unit_price_with_cost
        return self.unit_price

    def get_unit_price_readonly(self, name):
        return self._has_sale_unit_price_cost

    @classmethod
    def _get_origin(cls):
        return super()._get_origin() + ['sale.cost']

    @property
    def _has_sale_unit_price_cost(self):
        return bool(self.origin
            and isinstance(self.origin, Model)
            and self.origin.__name__ == 'sale.line'
            and self.origin.sale_price_cost_lines)

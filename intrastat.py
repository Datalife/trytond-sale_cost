# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.model import Model
from trytond.pool import PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @property
    def intrastat_amount(self):
        amount = super().intrastat_amount
        if (self.origin
                and isinstance(self.origin, Model)
                and self.origin.__name__ == 'sale.line'):
            cost = ((self.origin.costs_amount or Decimal('0.0')
                ) - (self.origin.indirect_costs_amount or Decimal('0.0')))
            shipped_qty = self.origin._get_shipped_quantity(
                self.shipment.__name__.split('.')[-1])
            if cost and shipped_qty:
                amount = self.origin.sale.currency.round(amount - (
                    cost * Decimal((self.internal_quantity / shipped_qty))))
        return amount

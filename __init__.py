# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import sale_cost
from . import intrastat
from . import sale
from . import invoice
from . import ir


def register():
    Pool.register(
        sale_cost.CostType,
        sale_cost.CostTemplate,
        sale_cost.SaleCost,
        sale_cost.CostLineSale,
        sale_cost.Sale,
        sale_cost.SaleLine,
        sale_cost.ProductCost,
        sale.Sale,
        sale.SaleLine,
        invoice.InvoiceLine,
        ir.Cron,
        sale_cost.SaleCostChangeFormulaStart,
        module='sale_cost', type_='model')
    Pool.register(
        sale.ModifyHeader,
        sale_cost.SaleCostChangeFormula,
        module='sale_cost', type_='wizard')
    Pool.register(
        sale_cost.Sale2,
        module='sale_cost', type_='model',
        depends=['sale_processing2confirmed'])
    Pool.register(
        intrastat.Move,
        module='sale_cost', type_='model',
        depends=['intrastat'])
    Pool.register(
        sale.Sale2,
        module='sale_cost', type_='model',
        depends=['analytic_sale'])
